package org.wiese2.twootsscheduler;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.wiese2.twootsscheduler.commands.CommandManager;
import org.wiese2.twootsscheduler.commands.TCommand;

import java.util.ArrayList;
import java.util.Arrays;

public class Main extends JavaPlugin {
    public static JavaPlugin instance = null;
    public CommandManager commandManager;

    @Override
    public void onEnable() {
        instance = this;
        commandManager = new CommandManager();
    }

    @Override
    public void onDisable() {
        commandManager.onDisable();
    }

    @Override
    public ArrayList<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if ((command.getName().equalsIgnoreCase("deltask") || command.getName().equalsIgnoreCase("showtask")) && args.length == 1) {
            return commandManager.listTasks();
        } else if (command.getName().equalsIgnoreCase("addtask")) {
            if (args.length == 1) {
                return list("name");
            } else if (args.length == 2) {
                return list("interval");
            } else if (args.length == 3) {
                return list("yes", "no");
            }
            return list();
        } else if (command.getName().equalsIgnoreCase("movetask")) {
            if (args.length == 1) {
                return commandManager.listTasks();
            } else if (args.length == 2) {
                TCommand cmd = commandManager.commands.get((new TCommand(args[0])).getHash());

                return cmd != null ? list(cmd.getInterval()+"") : list("interval");
            }
            return list();
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("addtask")) {
            return commandManager.doAddTask(sender, args);
        } else if (command.getName().equalsIgnoreCase("deltask")) {
            return commandManager.doDelTask(sender, args);
        } else if (command.getName().equalsIgnoreCase("tasks")) {
            return commandManager.doTasks(sender);
        } else if (command.getName().equalsIgnoreCase("showtask")) {
            return commandManager.doShowTask(sender, args);
        } else if (command.getName().equalsIgnoreCase("movetask")) {
            return commandManager.doMoveTask(sender, args);
        }
        return false;
    }

    public ArrayList<String> list(String ...args) {
        return new ArrayList<>(Arrays.asList(args));
    }
}
