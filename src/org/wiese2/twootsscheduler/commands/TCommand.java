package org.wiese2.twootsscheduler.commands;

import org.wiese2.helper.Hashing;
import org.wiese2.helper.IStorageSerializable;
import org.wiese2.helper.MapGetter;

import java.util.HashMap;
import java.util.Map;

public class TCommand extends IStorageSerializable {
    private String name;
    private String command;
    private int interval;
    private boolean runInitial;

    public TCommand() {}

    public TCommand(String name) {
        this.name = name;
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("command", command);
        map.put("interval", interval);
        map.put("runInitial", runInitial);
        return map;
    }

    @Override
    public IStorageSerializable deserialize(Map<String, Object> map) {
        String cmd = (String) map.get("command");
        String nam = (String) map.get("name");
        int intv = (int) map.get("interval");
        boolean runInitial = (boolean) MapGetter.get(map, "runInitial", false);
        return new TCommand(cmd, intv, nam, runInitial);
    }

    @Override
    public String getHash() {
        return Hashing.md5(name);
    }

    public TCommand(String command, int interval, String name, boolean runInitial) {
        this.command = command;
        this.interval = interval;
        this.name = name;
        this.runInitial = runInitial;
    }

    public String getCommand() {
        if (command.startsWith("/")) {
            command = command.replaceFirst("/", "");
        }

        return command.trim();
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public boolean isRunInitial() {
        return runInitial;
    }

    public void setRunInitial(boolean runInitial) {
        this.runInitial = runInitial;
    }
}
