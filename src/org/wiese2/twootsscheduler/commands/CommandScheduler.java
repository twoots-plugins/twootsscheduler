package org.wiese2.twootsscheduler.commands;

import org.wiese2.twootsscheduler.Main;

import java.util.HashMap;
import java.util.Map;

public class CommandScheduler {
    private final HashMap<String, CommandRunner> runners = new HashMap<>();

    public void schedule(TCommand command, boolean initialExecution) {
        CommandRunner runner = new CommandRunner(command);

        runner.runTaskTimer(Main.instance, initialExecution ? 0L : command.getInterval()*20L, command.getInterval()*20L);

        runners.put(command.getHash(), runner);
    }

    public void cancelAllRunners() {
        for (Map.Entry<String, CommandRunner> entry : runners.entrySet()) {
            CommandRunner runner = entry.getValue();

            runner.cancel();
        }
    }

    public void cancelRunner(TCommand command) {
        if (runners.containsKey(command.getHash())) {
            runners.get(command.getHash()).cancel();
            runners.remove(command.getHash());
        }
    }
}
