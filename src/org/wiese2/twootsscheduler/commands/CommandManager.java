package org.wiese2.twootsscheduler.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.wiese2.helper.Console;
import org.wiese2.helper.Storage;
import org.wiese2.twootsscheduler.Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommandManager {
    public CommandScheduler scheduler;
    public Storage<TCommand> commandStorage;
    public HashMap<String, TCommand> commands;

    public CommandManager() {
        commandStorage = new Storage<>(Main.instance, ".task", TCommand::new);
        commands = commandStorage.loadMapFromFiles();
        scheduler = new CommandScheduler();

        for (Map.Entry<String, TCommand> entry : commands.entrySet()) {
            TCommand cmd = entry.getValue();

            scheduler.schedule(cmd, cmd.isRunInitial());
        }
    }

    public void onDisable() {
        commandStorage.saveMapToFiles(commands);
    }

    public ArrayList<String> listTasks() {
        ArrayList<String> list = new ArrayList<>();
        for (Map.Entry<String, TCommand> entry : commands.entrySet()) {
            TCommand cmd = entry.getValue();

            list.add(cmd.getName());
        }
        return list;
    }

    public boolean doAddTask(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            if (args.length < 4) {
                Console.sendError(sender, "Usage: /addtask <name> <second-interval> <initial-execution> <command>");
                return true;
            }

            if (!args[2].equals("no") && !args[2].equals("yes")) {
                Console.sendError(sender, "Initial execution parameter has to be either 'yes' or 'no'");
                return true;
            }

            boolean initialExecution = args[2].equals("yes");

            int interval = -1;
            try {
                interval = Integer.parseInt(args[1]);
            } catch (NumberFormatException ignored) {}
            if (interval < 5 || interval > 60*60*6) {
                Console.sendError(sender, "Interval has to be between 5 seconds and 6 hours (21600 seconds)");
                return true;
            }

            String name = args[0];
            args[0] = args[1] = args[2] = "";
            String commandStr = String.join(" ", args).trim();

            TCommand command = new TCommand(commandStr, interval, name, initialExecution);
            commands.put(command.getHash(), command);

            scheduler.schedule(command, initialExecution);

            Console.sendSuccess(sender, name + " has been scheduled to run every " + interval + " seconds");
        }
        return true;
    }

    public boolean doDelTask(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            if (args.length < 1) {
                Console.sendError(sender, "Usage: /deltask <name>");
                return true;
            }

            String name = args[0];

            TCommand command = new TCommand("", 0, name, false);

            if (!commands.containsKey(command.getHash())) {
                Console.sendError(sender, "There is no task named " + name);
                return true;
            }

            commands.remove(command.getHash());
            commandStorage.remove(command);

            scheduler.cancelRunner(command);

            Console.sendSuccess(sender, name + " has been deleted");
        }
        return true;
    }

    public boolean doShowTask(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            if (args.length < 1) {
                Console.sendError(sender, "Usage: /showtask <name>");
                return true;
            }

            String name = args[0];

            TCommand command = new TCommand("", 0, name, false);

            if (!commands.containsKey(command.getHash())) {
                Console.sendError(sender, "There is no task named " + name);
                return true;
            }
            command = commands.get(command.getHash());

            String cmd = command.getCommand();
            if (cmd.length() > 25) {
                cmd = cmd.substring(0, 24) + "...";
            }

            Console.sendNotice(sender, "--- Task " + command.getName() + " ---");
            Console.sendNotice(sender, "Interval: " + command.getInterval());
            Console.sendNotice(sender, "Command: " + cmd);
            Console.sendNotice(sender, "Run Initially: " + (command.isRunInitial() ? "yes" : "no"));
        }
        return true;
    }

    public boolean doTasks(CommandSender sender) {
        if (sender instanceof Player) {
            ArrayList<String> list = listTasks();

            if (list.size() == 0) {
                Console.sendError(sender, "There are no scheduled commands");
            } else {
                Console.sendNotice(sender, "Tasks: " + String.join(", ", list.toArray(new String[]{})));
            }
        }
        return true;
    }

    public boolean doMoveTask(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            if (args.length < 2) {
                Console.sendError(sender, "Usage: /movetask <name> <second-interval>");
                return true;
            }

            String name = args[0];
            TCommand command = new TCommand("", 0, name, false);

            if (!commands.containsKey(command.getHash())) {
                Console.sendError(sender, "There is no task named " + name);
                return true;
            }

            int interval = -1;
            try {
                interval = Integer.parseInt(args[1]);
            } catch (NumberFormatException ignored) {}
            if (interval < 5 || interval > 60*60*6) {
                Console.sendError(sender, "Interval has to be between 5 seconds and 6 hours (21600 seconds)");
                return true;
            }

            command = commands.get(command.getHash());
            command.setInterval(interval);

            commands.put(command.getHash(), command);

            scheduler.cancelRunner(command);
            scheduler.schedule(command, command.isRunInitial());

            Console.sendSuccess(sender, name + " has been updated to run every " + interval + " seconds");
        }
        return true;
    }
}
