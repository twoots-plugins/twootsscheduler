package org.wiese2.twootsscheduler.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.scheduler.BukkitRunnable;

public class CommandRunner extends BukkitRunnable {
    private final TCommand command;

    public CommandRunner(TCommand command) {
        this.command = command;
    }

    @Override
    public void run() {
        System.out.println(command.getCommand());

        ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
        Bukkit.dispatchCommand(console, command.getCommand());
    }
}
