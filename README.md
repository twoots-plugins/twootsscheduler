## TwootsScheduler

Plugin to schedule commands.

### Usage

**Commands**

- `/addtask <name> <second-interval> <initial-execution> <command>`
    - Adds a scheduled command (task)
    - `<name>` is the name by which you can identify this task
    - `<second-interval>` is the interval in which the command should be run
        - has to be between 5 seconds and 6 hours (21600 seconds)
    - `<initial-execution>` if the command should be run as soon as the task is added or start after the interval
- `/deltask <name>`
    - Deletes the task named `<name>`
- `/tasks`
    - Lists all tasks
- `/showtask <name>`
    - Shows all infos about a certain task
- `/movetask <name> <second-interval>`
    - Changes the execution time of a task
    - `<name>` is the name by which you can identify this task
    - `<second-interval>` is the interval in which the command should be run
    
### Permissions

- `twootsscheduler.all`
    - Gives you access to all commands
    - Defaults to op only